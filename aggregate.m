function aggregate()
    [fData] = select_input_file();
    cols = select_cols(fData(1,:));
    prefixLength = select_aggregate_prefix(fData{2,1});
    fName = select_output_file();
    f = aggregate_fopen(fName);
    
    for col=cols
        [groups, nTrials] = aggregate_and_output(fData, col, prefixLength, f);
    end
    statusMsg = sprintf('Here are the groupings that were generated.\n\n');
    for ii=1:length(groups)
        statusMsg = [statusMsg, sprintf('%s* - %d trials\n', groups{ii}, nTrials(ii))];
    end
    helpdlg(statusMsg);
end

function [fileStrings] = select_input_file()
    [fName, fPath] = uigetfile('*', 'Select the file to aggregate.');
    if isequal(fName, 0)
        error('You cancelled the operation');
    else
        [fileStrings] = lines_to_cell(caseread([fPath fName]));
    end
end

function [cellArray] = lines_to_cell(lines)
    headerCell = textscan(lines(1,:), '%s', 'delimiter', '\t');
    cols = size(headerCell{1},1);
    cellArray = cell(size(lines,1), cols);
    for ii=1:size(lines,1)
        ts = textscan(lines(ii,:), '%s', 'delimiter', '\t');
        if length(ts{1})>cols
            ts{1} = ts{1}(1:cols);
        end
        row = cell(1, cols);
        row = init_empty_strings(row);
        row(1,1:length(ts{1})) = ts{1}';
        cellArray(ii,:) = row;
    end
end

function cellRow = init_empty_strings(cellRow)
    for ii=1:length(cellRow)
        cellRow{1,ii} = '';
    end
end

function cols = select_cols(colHeaders)
    [cols, ok] = listdlg('ListString', colHeaders, ...
                         'SelectionMode', 'multiple', ...
                         'Name', 'Select columns', ...
                         'PromptString', [{'Select the columns that you''d'} {'like to statistically analyze.'} {'Hold CTRL or SHIFT to select multiple columns.'}], ...
                         'ListSize', [250 300]);
    if ~ok
        cols = [];
    end
end

function prefixLength = select_aggregate_prefix(sampleFile)
    prefixes = cell(length(sampleFile)-4, 1);
    for ii=1:(length(sampleFile)-4)
        prefixes{ii} = [sampleFile(1:ii) '*'];
    end
    [iPrefix, ok] = listdlg('ListString', prefixes, ...
                            'SelectionMode', 'single', ...
                            'Name', 'Select aggregation', ...
                            'PromptString', 'Select the prefix to aggregate trials with.', ...
                            'ListSize', [250 300]);
    if ~ok
        error('You cancelled the operation.');
    end
    prefixLength = length(prefixes{iPrefix})-1;
end

function [groups, nTrials] = aggregate_and_output(inpData, iCol, prefixLength, fOut)
    colName = inpData{1, iCol};
    aggMap = containers.Map;
    for iRow=2:size(inpData, 1)
        thisFName = inpData{iRow, 1};
        thisPrefix = thisFName(1:prefixLength);
        if aggMap.isKey(thisPrefix)
            matchedValues = aggMap(thisPrefix);
        else
            matchedValues = [];
        end
        matchedValues = [matchedValues; str2double(inpData{iRow, iCol})];
        aggMap(thisPrefix) = matchedValues;
    end
    groups = aggMap.keys;
    nTrials = zeros(length(groups));
    for ii=1:length(groups)
        nTrials(ii) = length(aggMap(groups{ii}));

        vals = aggMap(groups{ii});
        minV = min(vals);
        maxV = max(vals);
        avgV = sum(vals)/length(vals);
        varV = var(vals);
        fprintf(fOut, '%s\t%s\t%f\t%f\t%f\t%f\n', groups{ii}, colName, minV, maxV, avgV, varV);
    end
end

function fName = select_output_file()
    defaultFname = ['aggregate_output_' datestr(now,'yyyy-mm-dd_HH-MM-SS') '.txt'];
    [name, path] = uiputfile('*', 'Select an output file.', defaultFname);
    fName = [path name];
end

function [f] = aggregate_fopen(fname)
    if exist(fname, 'file')
        f = fopen(fname, 'a');
        return;
    end
    f = fopen(fname, 'w');
    headers = {'Filename Match Aggregation', 'Selected column', 'Minimum', 'Maximum', 'Average', 'Variation'};
    for ii=1:length(headers);
        fprintf(f, '%s\t', headers{ii});
    end
    fprintf(f, '\n');
end