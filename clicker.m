function clicker()
    [fImport, fName, fPath] = select_input_file();
    colChoice = select_column(fImport.colheaders);
    [plotHandle, iClicks] = plot_col_and_clicks(fImport.data, colChoice);
    diffs = select_points_to_diff(length(iClicks));
    close(plotHandle);
    [addlCols, bAddlDiff] = select_additional_columns(fImport.colheaders);
    
    % open output file
    fOut = select_output_file(fPath);
    f = click_fopen(fOut, length(iClicks), diffs, length(addlCols), bAddlDiff);
    % write initial file
    write_click_file(f, fName, fImport, length(iClicks), colChoice, diffs, addlCols, false, bAddlDiff);
    % multiselect additional files
    % maybe loop this part? otherwise you're restricted to just one
    % directory.
    [fNames, path] = select_addl_file(fPath);
    for fName=fNames
        fImport = importdata([path fName{1}]);
        write_click_file(f, fName{1}, fImport, length(iClicks), colChoice, diffs, addlCols, true, bAddlDiff);
    end
    
    % close output file
    fclose(f);
end

function [fImport, fName, fPath] = select_input_file()
    [fName, fPath] = uigetfile('*', 'Select the first input file.');
    if isequal(fName, 0)
        error('You cancelled the operation');
    else
        fImport = importdata([fPath fName]);
    end
end

function [fNames, path] = select_addl_file(fPath)
    [fNames, path] = uigetfile('*', ...
        'Select additional input files. Use CTRL and SHIFT to select multiple.',...
        fPath,...
        'MultiSelect', 'on');
    if ~iscell(fNames)  % if there's only 1 file, wrap in a cell
        c = cell(1,1);
        c{1} = fNames;
        fNames = c;
    end
end
 
function colChoice = select_column(colHeaders)
    [colChoice, ok] = listdlg('ListString', colHeaders, ...
                              'SelectionMode', 'single', ...
                              'Name', 'Select initial column', ...
                              'PromptString', 'Select a column to process', ...
                              'ListSize', [250 300]);
    if ~ok
        error('You cancelled the operation');
    end
end

function [plotHandle, iClicks] = plot_col_and_clicks(fData, colChoice)
    iClicks = get_click_points(fData);
    plotHandle = figure;
    hold on;
    plot(fData(:,1), fData(:,colChoice));
    plot(fData(iClicks,1), fData(iClicks,colChoice), 'ro');
    for ii=1:length(iClicks)
        text(fData(iClicks(ii),1), fData(iClicks(ii),colChoice), sprintf(' \\leftarrow %d',ii));
    end
end

function diffs = select_points_to_diff(nClicks)
    allDiffs = zeros(nClicks*(nClicks-1)/2, 2);
    kk = 1;
    for ii=1:(nClicks-1)
        for jj=(ii+1):(nClicks)
            allDiffs(kk,:) = [ii jj];
            kk = kk + 1;
        end
    end
    choiceNames = cell(1,size(allDiffs,1));
    for ii=1:size(allDiffs,1)
        choiceNames{ii} = sprintf('Compare %d and %d', allDiffs(ii, 1), allDiffs(ii, 2));
    end
    [choices, ok] = listdlg('ListString', choiceNames, ...
                            'SelectionMode', 'multiple', ...
                            'Name', 'Select comparisons', ...
                            'PromptString', [{'Select the comparisons to make.'} {'Hold CTRL or SHIFT to select multiple comparisons.'}], ...
                            'ListSize', [250 300]);
    if ~ok
        error('You cancelled the operation');
    end
    diffs = allDiffs(choices,:);  % filter out only the ones that were selected
end

function [addlCols, diffOrAvg] = select_additional_columns(colHeaders)
    [addlCols, ok] = listdlg('ListString', colHeaders, ...
                             'SelectionMode', 'multiple', ...
                             'Name', 'Select additional columns', ...
                             'PromptString', [{'Select any additional columns'} {'to add to this analysis.'} {'Hold CTRL or SHIFT to select multiple comparisons.'}], ...
                             'ListSize', [250 300]);
    if ~ok
        addlCols = [];
    end
	diffOrAvg = zeros(size(addlCols));
	for ii=1:length(addlCols)
		choice = questdlg(['What would you like to do to this column? "' colHeaders{addlCols(ii)} '"'],...
						  'Additional column analysis',...
						  'Compute difference', 'Compute average over interval',...
						  'Compute difference');
		diffOrAvg(ii) = strcmp('Compute difference', choice);
	end
end

function fName = select_output_file(fPath)
    defaultFname = ['output_' datestr(now,'yyyy-mm-dd_HH-MM-SS') '.txt'];
    [name, path] = uiputfile('*', 'Select an output file. WARNING: Content will be overwritten.', [fPath defaultFname]);
    fName = [path name];
end

function write_click_file(fOut, fName, fImport, nPts, colChoice, diffs, addlCols, additionalFile, bAddlDiff)    
    iClicks = get_click_points(fImport.data);
    
    % if 'additionalFile' flag is set, show plot and allow user to reject
    % this trial
    if additionalFile
        fig = figure;
        hold on;
        plot(fImport.data(:, 1), fImport.data(:, colChoice));
        plot(fImport.data(iClicks,1), fImport.data(iClicks,colChoice), 'ro');
        choice = questdlg('Reject or keep this trial?',...
                          'Trial rejection selection', ...
                          'Keep','Reject', ...
                          'Keep');
        close(fig);
        if ~strcmp('Keep', choice)
            return;  % don't do anything for this file. on to the next one!
        end
    end
    
     % fOut is already open
    fprintf(fOut, '%s\t%s\t', fName, fImport.colheaders{colChoice});
    
    if length(iClicks) < nPts
        % there aren't enough clicks in this data set
        % generate empty data, TODO: confirm this is the correct behavior
        nBlanks = 2*nPts + 2*size(diffs,1) + length(addlCols)*(1+nPts+size(diffs,1));
        for ii=1:nBlanks
            fprintf(fOut, '\t');
        end
        fprintf(fOut, '\n');
        return;
    end
    for ii=1:nPts
        fprintf(fOut, '%f\t', fImport.data(iClicks(ii),1));
        fprintf(fOut, '%f\t', fImport.data(iClicks(ii),colChoice));
    end
    for ii=1:size(diffs,1)
        fprintf(fOut, '%f\t', fImport.data(iClicks(diffs(ii,1)),colChoice)-fImport.data(iClicks(diffs(ii,2)),colChoice));
    end
    for ii=1:size(diffs,1)
        fprintf(fOut, '%f\t', abs(fImport.data(iClicks(diffs(ii,1)),colChoice)-fImport.data(iClicks(diffs(ii,2)),colChoice)));
    end
    for jj=1:length(addlCols)
        col = addlCols(jj);
        fprintf(fOut, '%s\t', fImport.colheaders{col});
        for ii=1:nPts
            fprintf(fOut, '%f\t', fImport.data(iClicks(ii),col));
        end
        for ii=1:size(diffs,1)
            if bAddlDiff(jj)
                fprintf(fOut, '%f\t', fImport.data(iClicks(diffs(ii,1)),col)-fImport.data(iClicks(diffs(ii,2)),col));
            else
                % compute average over the interval
                sm = sum(fImport.data(iClicks(diffs(ii,1)):iClicks(diffs(ii,2)), col));
                avg = sm/(iClicks(diffs(ii,2))-iClicks(diffs(ii,1)));
                fprintf(fOut, '%f\t', avg);
            end
        end
    end
    fprintf(fOut, '\n');
end

function iClicks = get_click_points(fData)
    % first, find the column that has the switch data
    swCol = 1;
    for ii=size(fData, 2):-1:2  % start at the end because the button is likely the last column
        if max(fData(:,ii))==1 && min(fData(:,ii))==0
            swCol = ii;
            break;
        end
    end
    % find the rising edges, 25 frames (250 ms) is about as fast as you can
    % click the button
    [~, iClicks] = findpeaks(diff(fData(:,swCol)), 'MinPeakHeight', 0.99, 'MinPeakDistance', 25);
end

function [f] = click_fopen(fname, nPts, posDiffs, nAddlCols, bAddlDiff)
    if exist(fname, 'file')
        choice = questdlg('This will OVERWRITE the existing file. Continue?',...
                          'File overwrite warning', ...
                          'Continue','Abort', ...
                          'Continue');
        if ~strcmp('Continue', choice)
            error('You cancelled the operation');
        end
    end
    
    f = fopen(fname, 'w');
    headers = {'Filename', 'Selected column'};
    for ii=1:nPts
        headers = append_header(headers, sprintf('Pt%d frame', ii));
        headers = append_header(headers, sprintf('Pt%d value', ii));
    end
    % posDiffs = [1 2; 1 3; 1 5; 2 4; 3 4; 3 5; 4 5;];
    for ii=1:size(posDiffs, 1)
        headers = append_header(headers, sprintf('Diff (Pt%d - Pt%d)', posDiffs(ii,1), posDiffs(ii,2)));
    end
    for ii=1:size(posDiffs, 1)
        headers = append_header(headers, sprintf('Absolute diff (Pt%d - Pt%d)', posDiffs(ii,1), posDiffs(ii,2)));
    end
    for ii=1:nAddlCols  % number of additional columns
        headers = append_header(headers, sprintf('Add''l Col %d - Name', ii));
        for jj=1:nPts
            headers = append_header(headers, sprintf('Add''l Col %d - Pt%d Value', ii, jj));
        end
        if bAddlDiff(ii)
            for jj=1:size(posDiffs, 1)
                headers = append_header(headers, sprintf('Add''l Col %d - Diff (Pt%d - Pt%d)', ii, posDiffs(jj,1), posDiffs(jj,2)));
            end
        else
            for jj=1:size(posDiffs, 1)
                headers = append_header(headers, sprintf('Add''l Col %d - Avg b/t (Pt%d...Pt%d)', ii, posDiffs(jj,1), posDiffs(jj,2)));
            end 
        end
    end
    for ii=1:length(headers);
        fprintf(f, '%s\t', headers{ii});
    end
    fprintf(f, '\n');
end

function [headers] = append_header(headers, h)
    len = length(headers);
    headers{len+1} = h;
end