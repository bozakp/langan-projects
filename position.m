function position()
    config = {};
    config.colIndex = 1;
    config.windowTitle = 'Position - ';
    defaultFname = ['output_' datestr(now,'yyyy-mm-dd_HH-MM-SS') '.txt'];
    [config.oFname,config.oDir] = uiputfile('*', 'Select output file (existing or new)', defaultFname);
    config.inpDir = config.oDir;  %initial guess for input file directory is output file directory
    while 1
        config = run_one_file(config);
        choice = questdlg('Would you like to process another file?',...
                          [config.windowTitle 'Continue?'], ...
                          'Yes','No', ...
                          'Yes');
        if ~strcmp('Yes', choice)
            break
        end
    end
end

function [config] = run_one_file(config)
    [inpFname,config.inpDir] = uigetfile('*', 'Select a file to load data', config.inpDir);
    fImport = importdata([config.inpDir inpFname]);
    
    % Analyze the first column selected
    choice = select_column_from_list(fImport.colheaders, config.colIndex, [config.windowTitle 'Column Select']);

    fData = fImport.data;
    [iFrames, iVFrames] = auto_find_points(fData(:,choice));  % automatically choose peaks
    ok = confirm_auto_points(fData(:,1), fData(:,choice), iFrames);
    if ~ok
        iFrames = select_peaks(fData(:,1), fData(:,choice), fImport.colheaders{choice});
    end
    if length(iFrames) < 5
        % not enough points. generate zero-ed data
        posFrames = zeros(5,1);
        positions = zeros(5,1);
        posDiff = zeros(5,5);
    else
        posFrames = fData(iFrames,1);
        [positions, posDiff] = get_positional_diffs(iFrames, fData(:,choice));
    end
    
    ok = confirm_auto_points(fData(:,1), fData(:,choice), iVFrames);
    if ~ok
        iVFrames = select_velocities(fData(:,1), fData(:,choice));
    end
    if length(iVFrames) < 8
        % not enough points. generate zero-ed data
        velFrames = zeros(8,1);
        velPositions = zeros(8,1);
        velocities = zeros(4,1);
        holds = zeros(3,1);
    else
        velFrames = fData(iVFrames,1);
        velPositions = fData(iVFrames,choice);
        velocities = get_velocities(velFrames, velPositions);
        holds = get_holds(velFrames);
    end
    
    f = position_fopen(config.oFname);
    fprintf(f, '%s\t%s\t', inpFname, fImport.colheaders{choice});
    for ii=1:5
        fprintf(f, '%f\t%f\t', posFrames(ii), positions(ii));
    end
    pos_diffs = [1 2; 1 3; 1 5; 2 4; 3 4; 3 5; 4 5;];
    for ii=1:size(pos_diffs,1)
        fprintf(f, '%f\t', posDiff(pos_diffs(ii,1), pos_diffs(ii,2)));
    end
    for ii=1:size(pos_diffs,1)
        fprintf(f, '%f\t', abs(posDiff(pos_diffs(ii,1), pos_diffs(ii,2))));
    end
    for ii=1:8
        fprintf(f, '%f\t%f\t', velFrames(ii), velPositions(ii));
    end
    for ii=1:4
        fprintf(f, '%f\t', velocities(ii)*100);  % scaling factor (100 Hz)
    end
    for ii=1:3
        fprintf(f, '%f\t', holds(ii)/100);  % scaling factor (100 Hz)
    end
    
    % Ask if any additional columns need to be processed
    while 1
        config.colIndex = choice; 
        fImport.colheaders{choice} = ['[DONE] - ' fImport.colheaders{choice}];
        answer = questdlg('Would you like to process another column in this file?',...
                          [config.windowTitle 'Continue?'], ...
                          'Yes','No', ...
                          'Yes');
        if ~strcmp('Yes', answer)
            break
        end
        choice = select_column_from_list(fImport.colheaders, config.colIndex, [config.windowTitle 'Column Select']);
        % display graph to confirm points, etc.
        confirmFig = figure;
        set(confirmFig, 'WindowStyle', 'modal');
        hold on;
        plot(fData(:,1), fData(:,choice));
        plot(fData(iFrames,1), fData(iFrames,choice), 'ro');
        title('Close this window after you have visually inspected the data from this column');
        ylabel(fImport.colheaders{choice});
        waitfor(confirmFig);
        
        fprintf(f, '%s\t', fImport.colheaders{choice});
        [positions, posDiff] = get_positional_diffs(iFrames, fData(:,choice));
        for ii=1:5
            fprintf(f, '%f\t%f\t', posFrames(ii), positions(ii));
        end
        pos_diffs = [1 2; 1 3; 1 5; 2 4; 3 4; 3 5; 4 5;];
        for ii=1:size(pos_diffs,1)
            fprintf(f, '%f\t', posDiff(pos_diffs(ii,1), pos_diffs(ii,2)));
        end
    end
    fprintf(f, '\n');
    fclose(f);
end

function [choice] = select_column_from_list(list, initial, title)
    [choice, ok] = listdlg('ListString', list, ...
                           'SelectionMode', 'single', ...
                           'InitialValue', initial, ...
                           'Name', title, ...
                           'PromptString', 'Select a column to process', ...
                           'ListSize', [250 300]);
	if ~ok
        error('You cancelled the operation');
    end
end

function [xIndices] = select_peaks(xData, yData, plotTitle)
    plotH = figure;
    set(plotH, 'WindowStyle', 'modal');
    plot(xData, yData);
    title({'Select the 5 positional peaks.','If there aren''t 5 points, just hit ENTER.'});
    ylabel(plotTitle);
    [ptX, ~] = ginput(5);  % TODO: use nearest pt by distance?
    ptX = sort(ptX);  % just to confirm that they're in order
    close(plotH);
    xIndices = nearest_indicies_in_vector(xData, ptX);
end

function [yPts, diffMatrix] = get_positional_diffs(iSelection, yData)
    yPts = yData(iSelection);
    diffMatrix = get_diff_matrix(yPts);
end

function [iFrames] = select_velocities(xData, yData)
    plotH = figure;
    set(plotH, 'WindowStyle', 'modal');
    plot(xData, yData);
    title({'Select 8 points. One at the beginning and end of each slope.','If there aren''t 8 points, just hit ENTER on the plot.'});
    [ptX, ~] = ginput(8);  % TODO: use nearest pt by distance?
    ptX = sort(ptX);  % just to confirm that they're in order
    iFrames = nearest_indicies_in_vector(xData, ptX);
    close(plotH);
end

function inds = nearest_indicies_in_vector(vec, values)
    inds = values;
    for ii=1:length(values)
        [~, inds(ii)] = min(abs(vec - values(ii)));  
    end
end

function dm = get_diff_matrix(vec)
    vecCol = vec;
    if size(vec,2) > size(vec,1)
        vecCol = vec';
    end
    copyRow = repmat(vecCol, 1, length(vecCol));
    copyCol = repmat(vecCol', length(vecCol), 1);
    dm = copyRow - copyCol;
end

function vs = get_velocities(xs, ys)
    vs = zeros(length(xs)/2, 1);
    for ii=2:2:length(xs)
        vs(ii/2) = (ys(ii)-ys(ii-1))/(xs(ii)-xs(ii-1));
    end
end

function hs = get_holds(xs)
    hs = zeros(3, 1);
    hs(1) = xs(3) - xs(2);
    hs(2) = xs(5) - xs(4);
    hs(3) = xs(7) - xs(6);
end

function [f] = position_fopen(fname)
    if ~exist(fname, 'file')
        f = fopen(fname, 'w');
        headers = {'Filename', 'Selected column'};
        for ii=1:5
            headers = append_header(headers, sprintf('Peak%d frame', ii));
            headers = append_header(headers, sprintf('Peak%d value', ii));
        end
        pos_diffs = [1 2; 1 3; 1 5; 2 4; 3 4; 3 5; 4 5;];
        for ii=1:size(pos_diffs, 1)
            headers = append_header(headers, sprintf('Diff (Peak%d - Peak%d)', pos_diffs(ii,1), pos_diffs(ii,2)));
        end
        for ii=1:size(pos_diffs, 1)
            headers = append_header(headers, sprintf('Absolute diff (Peak%d - Peak%d)', pos_diffs(ii,1), pos_diffs(ii,2)));
        end
        for ii=1:4
            headers = append_header(headers, sprintf('Vel%d start frame', ii));
            headers = append_header(headers, sprintf('Vel%d start value', ii));
            headers = append_header(headers, sprintf('Vel%d end frame', ii));
            headers = append_header(headers, sprintf('Vel%d end value', ii));
        end
        for ii=1:4
            headers = append_header(headers, sprintf('Vel%d velocity', ii));
        end
        for ii=1:3
            headers = append_header(headers, sprintf('Hold time %d', ii));
        end
        for ii=1:7  % number of additional columns
            headers = append_header(headers, sprintf('Add''l Col %d - Name', ii));
            for jj=1:5
                headers = append_header(headers, sprintf('Add''l Col %d - Peak%d Value', ii, jj));
            end
            for jj=1:size(pos_diffs, 1)
                headers = append_header(headers, sprintf('Add''l Col %d - Diff (Peak%d - Peak%d)', ii, pos_diffs(jj,1), pos_diffs(jj,2)));
            end
        end
        for ii=1:length(headers);
            fprintf(f, '%s\t', headers{ii});
        end
        fprintf(f, '\n');
    else
        f = fopen(fname, 'a');
    end
end

function [headers] = append_header(headers, h)
    len = length(headers);
    headers{len+1} = h;
end

function [iPeaks, iVelStartEnds] = auto_find_points(yData)
    % find slopes
    ySmooth = smooth(yData, 0.05, 'lowess');
    yPrime = diff(ySmooth);
    yPrimeNorm = yPrime./max(abs(yPrime));
    [~, iSlopes] = findpeaks(abs(yPrimeNorm), 'MinPeakHeight', 0.35, 'NPeaks', 4);
    slopes = yPrime(iSlopes);
    
    % get peaks
    if slopes(1) > 0  % is this the correct criteria to invert the data?
        ySmooth = -ySmooth;
        slopes = -slopes;
    end
    [~, pk1] = max(ySmooth(1:iSlopes(1)));
    [~, pk2] = min(ySmooth(iSlopes(1):iSlopes(2)));
    [~, pk3] = max(ySmooth(iSlopes(2):iSlopes(3)));
    [~, pk4] = min(ySmooth(iSlopes(3):iSlopes(4)));
    [~, pk5] = max(ySmooth(iSlopes(4):end));
    iPeaks = [pk1; iSlopes(1)+pk2; iSlopes(2)+pk3; iSlopes(3)+pk4; iSlopes(4)+pk5];
    
    % get velocity starts/ends
    %my_intersection = @(nPk, nSlope) ((ySmooth(iPeaks(nPk))-ySmooth(iSlopes(nSlope)))/slopes(nSlope) + iSlopes(nSlope));
    iVelStartEnds = [get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 1, 1); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 2, 1); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 2, 2); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 3, 2); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 3, 3); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 4, 3); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 4, 4); ...
                     get_slope_start(ySmooth, iPeaks, iSlopes, slopes, 5, 4);];
%     firstGuess = round(firstGuess);
%     my_slope = @(x0,x1) ((ySmooth(x1)-ySmooth(x0))./(x1-x0));
%     firstSlopes = my_slope(firstGuess(1:2:end), firstGuess(2:2:end));
%     my_better_intersection = @(nStEnd, nPk) ((ySmooth(iPeaks(nPk))-ySmooth(firstGuess(nStEnd)))/firstSlopes((nStEnd+1)/2)+firstGuess(nStEnd));
%     iVelStartEnds = [my_better_intersection(1, 1); ...
%                      my_better_intersection(1, 2); ...
%                      my_better_intersection(3, 2); ...
%                      my_better_intersection(3, 3); ...
%                      my_better_intersection(5, 3); ...
%                      my_better_intersection(5, 4); ...
%                      my_better_intersection(7, 4); ...
%                      my_better_intersection(7, 5);];
%     iVelStartEnds = round(iVelStartEnds);
end

function [iSlopeStart] = get_slope_start(ySmooth, iPeaks, iSlopes, slopes, nPk, nSlope)
    iIntersection = ((ySmooth(iPeaks(nPk))-ySmooth(iSlopes(nSlope)))/slopes(nSlope) + iSlopes(nSlope));
    iMinDist = iPeaks(nPk);
    dist = @(x) (norm([(iIntersection-x) (ySmooth(iPeaks(nPk))-ySmooth(x))],2));
    if iPeaks(nPk) < iSlopes(nSlope)
        pts = iPeaks(nPk):iSlopes(nSlope);
    else
        pts = iSlopes(nSlope):iPeaks(nPk);
    end
    for ii=pts
        if dist(ii) < dist(iMinDist)
            iMinDist = ii;
        end
    end
    iSlopeStart = iMinDist;
end

function ok = confirm_auto_points(xData, yData, iPts)
    fig = figure;
    set(fig, 'WindowStyle', 'modal');
    hold on;
    plot(xData, yData);
    plot(xData(iPts), yData(iPts), 'ro');
    choice = questdlg('I have automatically determined the points. Is this good?',...
                      'Automatic point selection', ...
                      'Yes, good!','No. Do manual selection.', ...
                      'Yes, good!');
    close(fig);
    ok = strcmp('Yes, good!', choice);
end
